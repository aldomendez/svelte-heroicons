import { academicCap } from "./Icons/academic-cap";
import { adjustments } from "./Icons/adjustments";
import { annotation } from "./Icons/annotation";
import { archive } from "./Icons/archive";
import { arrowCircleDown } from "./Icons/arrow-circle-down";
import { arrowCircleLeft } from "./Icons/arrow-circle-left";
import { arrowCircleRight } from "./Icons/arrow-circle-right";
import { arrowCircleUp } from "./Icons/arrow-circle-up";
import { arrowDown } from "./Icons/arrow-down";
import { arrowLeft } from "./Icons/arrow-left";
import { arrowNarrowDown } from "./Icons/arrow-narrow-down";
import { arrowNarrowLeft } from "./Icons/arrow-narrow-left";
import { arrowNarrowRight } from "./Icons/arrow-narrow-right";
import { arrowNarrowUp } from "./Icons/arrow-narrow-up";
import { arrowRight } from "./Icons/arrow-right";
import { arrowUp } from "./Icons/arrow-up";
import { arrowsExpand } from "./Icons/arrows-expand";
import { atSymbol } from "./Icons/at-symbol";
import { backspace } from "./Icons/backspace";
import { badgeCheck } from "./Icons/badge-check";
import { ban } from "./Icons/ban";
import { beaker } from "./Icons/beaker";
import { bell } from "./Icons/bell";
import { bookOpen } from "./Icons/book-open";
import { bookmarkAlt } from "./Icons/bookmark-alt";
import { bookmark } from "./Icons/bookmark";
import { briefcase } from "./Icons/briefcase";
import { cake } from "./Icons/cake";
import { calculator } from "./Icons/calculator";
import { calendar } from "./Icons/calendar";
import { camera } from "./Icons/camera";
import { cash } from "./Icons/cash";
import { chartBar } from "./Icons/chart-bar";
import { chartPie } from "./Icons/chart-pie";
import { chartSquareBar } from "./Icons/chart-square-bar";
import { chatAlt2 } from "./Icons/chat-alt-2";
import { chatAlt } from "./Icons/chat-alt";
import { chat } from "./Icons/chat";
import { checkCircle } from "./Icons/check-circle";
import { check } from "./Icons/check";
import { chevronDoubleDown } from "./Icons/chevron-double-down";
import { chevronDoubleLeft } from "./Icons/chevron-double-left";
import { chevronDoubleRight } from "./Icons/chevron-double-right";
import { chevronDoubleUp } from "./Icons/chevron-double-up";
import { chevronDown } from "./Icons/chevron-down";
import { chevronLeft } from "./Icons/chevron-left";
import { chevronRight } from "./Icons/chevron-right";
import { chevronUp } from "./Icons/chevron-up";
import { chip } from "./Icons/chip";
import { clipboardCheck } from "./Icons/clipboard-check";
import { clipboardCopy } from "./Icons/clipboard-copy";
import { clipboardList } from "./Icons/clipboard-list";
import { clipboard } from "./Icons/clipboard";
import { clock } from "./Icons/clock";
import { cloudDownload } from "./Icons/cloud-download";
import { cloudUpload } from "./Icons/cloud-upload";
import { cloud } from "./Icons/cloud";
import { code } from "./Icons/code";
import { cog } from "./Icons/cog";
import { collection } from "./Icons/collection";
import { colorSwatch } from "./Icons/color-swatch";
import { creditCard } from "./Icons/credit-card";
import { cubeTransparent } from "./Icons/cube-transparent";
import { cube } from "./Icons/cube";
import { currencyBangladeshi } from "./Icons/currency-bangladeshi";
import { currencyDollar } from "./Icons/currency-dollar";
import { currencyEuro } from "./Icons/currency-euro";
import { currencyPound } from "./Icons/currency-pound";
import { currencyRupee } from "./Icons/currency-rupee";
import { currencyYen } from "./Icons/currency-yen";
import { cursorClick } from "./Icons/cursor-click";
import { database } from "./Icons/database";
import { desktopComputer } from "./Icons/desktop-computer";
import { deviceMobile } from "./Icons/device-mobile";
import { deviceTablet } from "./Icons/device-tablet";
import { documentAdd } from "./Icons/document-add";
import { documentDownload } from "./Icons/document-download";
import { documentDuplicate } from "./Icons/document-duplicate";
import { documentRemove } from "./Icons/document-remove";
import { documentReport } from "./Icons/document-report";
import { documentSearch } from "./Icons/document-search";
import { documentText } from "./Icons/document-text";
import { document } from "./Icons/document";
import { dotsCircleHorizontal } from "./Icons/dots-circle-horizontal";
import { dotsHorizontal } from "./Icons/dots-horizontal";
import { dotsVertical } from "./Icons/dots-vertical";
import { download } from "./Icons/download";
import { duplicate } from "./Icons/duplicate";
import { emojiHappy } from "./Icons/emoji-happy";
import { emojiSad } from "./Icons/emoji-sad";
import { exclamationCircle } from "./Icons/exclamation-circle";
import { exclamation } from "./Icons/exclamation";
import { externalLink } from "./Icons/external-link";
import { eyeOff } from "./Icons/eye-off";
import { eye } from "./Icons/eye";
import { fastForward } from "./Icons/fast-forward";
import { film } from "./Icons/film";
import { filter } from "./Icons/filter";
import { fingerPrint } from "./Icons/finger-print";
import { fire } from "./Icons/fire";
import { flag } from "./Icons/flag";
import { folderAdd } from "./Icons/folder-add";
import { folderDownload } from "./Icons/folder-download";
import { folderOpen } from "./Icons/folder-open";
import { folderRemove } from "./Icons/folder-remove";
import { folder } from "./Icons/folder";
import { gift } from "./Icons/gift";
import { globeAlt } from "./Icons/globe-alt";
import { globe } from "./Icons/globe";
import { hand } from "./Icons/hand";
import { hashtag } from "./Icons/hashtag";
import { heart } from "./Icons/heart";
import { home } from "./Icons/home";
import { identification } from "./Icons/identification";
import { inboxIn } from "./Icons/inbox-in";
import { inbox } from "./Icons/inbox";
import { informationCircle } from "./Icons/information-circle";
import { key } from "./Icons/key";
import { library } from "./Icons/library";
import { lightBulb } from "./Icons/light-bulb";
import { lightningBolt } from "./Icons/lightning-bolt";
import { link } from "./Icons/link";
import { locationMarker } from "./Icons/location-marker";
import { lockClosed } from "./Icons/lock-closed";
import { lockOpen } from "./Icons/lock-open";
import { login } from "./Icons/login";
import { logout } from "./Icons/logout";
import { mailOpen } from "./Icons/mail-open";
import { mail } from "./Icons/mail";
import { map } from "./Icons/map";
import { menuAlt1 } from "./Icons/menu-alt-1";
import { menuAlt2 } from "./Icons/menu-alt-2";
import { menuAlt3 } from "./Icons/menu-alt-3";
import { menuAlt4 } from "./Icons/menu-alt-4";
import { menu } from "./Icons/menu";
import { microphone } from "./Icons/microphone";
import { minusCircle } from "./Icons/minus-circle";
import { minusSm } from "./Icons/minus-sm";
import { minus } from "./Icons/minus";
import { moon } from "./Icons/moon";
import { musicNote } from "./Icons/music-note";
import { newspaper } from "./Icons/newspaper";
import { officeBuilding } from "./Icons/office-building";
import { paperAirplane } from "./Icons/paper-airplane";
import { paperClip } from "./Icons/paper-clip";
import { pause } from "./Icons/pause";
import { pencilAlt } from "./Icons/pencil-alt";
import { pencil } from "./Icons/pencil";
import { phoneIncoming } from "./Icons/phone-incoming";
import { phoneMissedCall } from "./Icons/phone-missed-call";
import { phoneOutgoing } from "./Icons/phone-outgoing";
import { phone } from "./Icons/phone";
import { photograph } from "./Icons/photograph";
import { play } from "./Icons/play";
import { plusCircle } from "./Icons/plus-circle";
import { plusSm } from "./Icons/plus-sm";
import { plus } from "./Icons/plus";
import { presentationChartBar } from "./Icons/presentation-chart-bar";
import { presentationChartLine } from "./Icons/presentation-chart-line";
import { printer } from "./Icons/printer";
import { puzzle } from "./Icons/puzzle";
import { qrcode } from "./Icons/qrcode";
import { questionMarkCircle } from "./Icons/question-mark-circle";
import { receiptRefund } from "./Icons/receipt-refund";
import { receiptTax } from "./Icons/receipt-tax";
import { refresh } from "./Icons/refresh";
import { reply } from "./Icons/reply";
import { rewind } from "./Icons/rewind";
import { rss } from "./Icons/rss";
import { saveAs } from "./Icons/save-as";
import { save } from "./Icons/save";
import { scale } from "./Icons/scale";
import { scissors } from "./Icons/scissors";
import { searchCircle } from "./Icons/search-circle";
import { search } from "./Icons/search";
import { selector } from "./Icons/selector";
import { server } from "./Icons/server";
import { share } from "./Icons/share";
import { shieldCheck } from "./Icons/shield-check";
import { shieldExclamation } from "./Icons/shield-exclamation";
import { shoppingBag } from "./Icons/shopping-bag";
import { shoppingCart } from "./Icons/shopping-cart";
import { sortAscending } from "./Icons/sort-ascending";
import { sortDescending } from "./Icons/sort-descending";
import { sparkles } from "./Icons/sparkles";
import { speakerphone } from "./Icons/speakerphone";
import { star } from "./Icons/star";
import { statusOffline } from "./Icons/status-offline";
import { statusOnline } from "./Icons/status-online";
import { stop } from "./Icons/stop";
import { sun } from "./Icons/sun";
import { support } from "./Icons/support";
import { switchHorizontal } from "./Icons/switch-horizontal";
import { switchVertical } from "./Icons/switch-vertical";
import { table } from "./Icons/table";
import { tag } from "./Icons/tag";
import { template } from "./Icons/template";
import { terminal } from "./Icons/terminal";
import { thumbDown } from "./Icons/thumb-down";
import { thumbUp } from "./Icons/thumb-up";
import { ticket } from "./Icons/ticket";
import { translate } from "./Icons/translate";
import { trash } from "./Icons/trash";
import { trendingDown } from "./Icons/trending-down";
import { trendingUp } from "./Icons/trending-up";
import { truck } from "./Icons/truck";
import { upload } from "./Icons/upload";
import { userAdd } from "./Icons/user-add";
import { userCircle } from "./Icons/user-circle";
import { userGroup } from "./Icons/user-group";
import { userRemove } from "./Icons/user-remove";
import { user } from "./Icons/user";
import { users } from "./Icons/users";
import { variable } from "./Icons/variable";
import { videoCamera } from "./Icons/video-camera";
import { viewBoards } from "./Icons/view-boards";
import { viewGridAdd } from "./Icons/view-grid-add";
import { viewGrid } from "./Icons/view-grid";
import { viewList } from "./Icons/view-list";
import { volumeOff } from "./Icons/volume-off";
import { volumeUp } from "./Icons/volume-up";
import { wifi } from "./Icons/wifi";
import { xCircle } from "./Icons/x-circle";
import { x } from "./Icons/x";
import { zoomIn } from "./Icons/zoom-in";
import { zoomOut } from "./Icons/zoom-out";
export {
    academicCap,
    adjustments,
    annotation,
    archive,
    arrowCircleDown,
    arrowCircleLeft,
    arrowCircleRight,
    arrowCircleUp,
    arrowDown,
    arrowLeft,
    arrowNarrowDown,
    arrowNarrowLeft,
    arrowNarrowRight,
    arrowNarrowUp,
    arrowRight,
    arrowUp,
    arrowsExpand,
    atSymbol,
    backspace,
    badgeCheck,
    ban,
    beaker,
    bell,
    bookOpen,
    bookmarkAlt,
    bookmark,
    briefcase,
    cake,
    calculator,
    calendar,
    camera,
    cash,
    chartBar,
    chartPie,
    chartSquareBar,
    chatAlt2,
    chatAlt,
    chat,
    checkCircle,
    check,
    chevronDoubleDown,
    chevronDoubleLeft,
    chevronDoubleRight,
    chevronDoubleUp,
    chevronDown,
    chevronLeft,
    chevronRight,
    chevronUp,
    chip,
    clipboardCheck,
    clipboardCopy,
    clipboardList,
    clipboard,
    clock,
    cloudDownload,
    cloudUpload,
    cloud,
    code,
    cog,
    collection,
    colorSwatch,
    creditCard,
    cubeTransparent,
    cube,
    currencyBangladeshi,
    currencyDollar,
    currencyEuro,
    currencyPound,
    currencyRupee,
    currencyYen,
    cursorClick,
    database,
    desktopComputer,
    deviceMobile,
    deviceTablet,
    documentAdd,
    documentDownload,
    documentDuplicate,
    documentRemove,
    documentReport,
    documentSearch,
    documentText,
    document,
    dotsCircleHorizontal,
    dotsHorizontal,
    dotsVertical,
    download,
    duplicate,
    emojiHappy,
    emojiSad,
    exclamationCircle,
    exclamation,
    externalLink,
    eyeOff,
    eye,
    fastForward,
    film,
    filter,
    fingerPrint,
    fire,
    flag,
    folderAdd,
    folderDownload,
    folderOpen,
    folderRemove,
    folder,
    gift,
    globeAlt,
    globe,
    hand,
    hashtag,
    heart,
    home,
    identification,
    inboxIn,
    inbox,
    informationCircle,
    key,
    library,
    lightBulb,
    lightningBolt,
    link,
    locationMarker,
    lockClosed,
    lockOpen,
    login,
    logout,
    mailOpen,
    mail,
    map,
    menuAlt1,
    menuAlt2,
    menuAlt3,
    menuAlt4,
    menu,
    microphone,
    minusCircle,
    minusSm,
    minus,
    moon,
    musicNote,
    newspaper,
    officeBuilding,
    paperAirplane,
    paperClip,
    pause,
    pencilAlt,
    pencil,
    phoneIncoming,
    phoneMissedCall,
    phoneOutgoing,
    phone,
    photograph,
    play,
    plusCircle,
    plusSm,
    plus,
    presentationChartBar,
    presentationChartLine,
    printer,
    puzzle,
    qrcode,
    questionMarkCircle,
    receiptRefund,
    receiptTax,
    refresh,
    reply,
    rewind,
    rss,
    saveAs,
    save,
    scale,
    scissors,
    searchCircle,
    search,
    selector,
    server,
    share,
    shieldCheck,
    shieldExclamation,
    shoppingBag,
    shoppingCart,
    sortAscending,
    sortDescending,
    sparkles,
    speakerphone,
    star,
    statusOffline,
    statusOnline,
    stop,
    sun,
    support,
    switchHorizontal,
    switchVertical,
    table,
    tag,
    template,
    terminal,
    thumbDown,
    thumbUp,
    ticket,
    translate,
    trash,
    trendingDown,
    trendingUp,
    truck,
    upload,
    userAdd,
    userCircle,
    userGroup,
    userRemove,
    user,
    users,
    variable,
    videoCamera,
    viewBoards,
    viewGridAdd,
    viewGrid,
    viewList,
    volumeOff,
    volumeUp,
    wifi,
    xCircle,
    x,
    zoomIn,
    zoomOut
};